#include <stdio.h>

float fToc(float f);

int main(){

    float fahr;
    float lower, upper, step;

    lower = 0.f;
    upper = 300.f;
    step = 20.f;

    printf("fahr to celsius\n");
    fahr = lower;
    while(fahr<=upper){
        float a = fToc(fahr);
        printf("%3.0f %6.1f\n", fahr, a);
        fahr = fahr + step;
    }
    
}

float fToc(float f){
    float c = (5.0f/9.0f)*(f-32.0f);
    return c;
}