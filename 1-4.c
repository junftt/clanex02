#include <stdio.h>

int main(){
    float fahr, celsius;
    int lower, upper, step;

    lower = -18;
    upper = 150;
    step = 10;

    printf("celsius to fahr\n");
    celsius = lower;
    while(celsius<=upper){
        fahr = celsius/(5.0/9.0)+32.0;
        printf("%5.1f %8.0f\n", celsius, fahr);
        celsius = celsius + step;
    }    
}