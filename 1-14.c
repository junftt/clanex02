#include <stdio.h>
#define MAX 128
/*
asfffaf234124,./;'[]][]./.~~~~!@#$%^&*()_+!@#$%^&*()_+{}|            

*/
int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    
    int len_arr[MAX];
    int max = 0;
    for(int i=0; i<MAX; ++i){
        len_arr[i] = 0;
    }

    char cr;
    while ((cr=getchar())!=EOF){
        ++len_arr[cr];
        if(len_arr[cr]>max){
            max = len_arr[cr];
        }
    }
    for(int i=1; i<=max; ++i){
        for(int j=0; j<MAX; ++j){
            if(len_arr[j]>0){
                if((max-i)<len_arr[j]){
                    putchar('*');
                }else{
                    putchar(' ');
                }
            }
        }
        putchar('\n');
    }
    for(int j=0; j<MAX; ++j){
        if(len_arr[j]>0){
            putchar(j);
        }
    }
}