#include <stdio.h>

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    int cr;
    int a=0, b=0, c=0;
    while ((cr = getchar())!=EOF)
    {
        if(cr == ' '){
            ++a;
        }else if(cr == '\t'){
            ++b;
        }else if(cr == '\n'){
            ++c;
        }
    }
    printf("%d\n%d\n%d\n",a,b,c);
    return 0;
}
