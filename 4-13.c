#include <stdio.h>

void reverse(char s[], int i, int len){
    int a;
    int b = len-i-1;
    if(i<b){
        a = s[i];
        s[i] = s[b];
        s[b] = a;
        reverse(s, ++i, len);
    }
}

int main(){
    char s[] = "abcde";
    reverse(s,0,5);
    printf("%s",s);
}