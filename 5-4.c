#include <stdio.h>

int strend(char *s, char *t){
    char *s_0 = s;
    char *t_0 = t;
    while (*s++ != '\0');
    --s;
    while (*t++ != '\0');
    --t;

    while(s>=s_0&&t>=t_0 && *s--==*t--);
    return t<t_0;
}

int main(){
    char *s = "abcdefg";
    char *t = "efg";
    printf("%d",strend(s,t));
    return 0;
}