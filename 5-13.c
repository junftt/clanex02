#include <stdio.h>
#define MAXFULLLINE 100
#define SMALL 10
#define MAXCOL 80

void printNLines(int n);
int atoi(char *s);
int getline_(char *s, int lim);

char full_lines[MAXFULLLINE][MAXCOL];

int main(int argc, char *argv[]){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    printNLines(atoi(argv[1]));
    return 0;
}

int atoi(char *s){
    int i = 0;
    int n = 0;
    while(s[i]!='\0'){
        if(s[i] >= '0' && s[i] <= '9'){
            n = 10 * n + (s[i] - '0');
        }
        ++i;
    }
    return n;
}

int getline_(char s[], int lim){
    int c, i;
    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    if (c == '\n')
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

void printNLines(int n){
    int line_num = 0;
    while(getline_(full_lines[line_num], MAXCOL)){
        ++line_num;
    }
    int index = 0;
    while(index<line_num){
        if((line_num - index)<=n){
            printf("%s", full_lines[index]);
        }
        ++index;
    }
}