#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define MAXWORDLEN 20
#define MAXLINENUM 20
#define MAXWORDCOUNT 100
/*
play carti carti plug plug plug
play carti plug
play plug
plug

carti 3
play 3
plug 6
*/
int line_index = 1;
char c = ' ';

int getword(char w[MAXWORDLEN])
{
    int index = 0;
    while (!isalpha(c)){
        if(c == EOF){
            return -1;
        }else if(c == '\n'){
            ++line_index;
        }
        c = getchar();
    }

    w[index++] = c;
    while (isalpha(c = getchar())){
        w[index++] = c;
    }
    w[index] = '\0';
    return 1;
}

struct tnode{
    char *word;
    int count;
    struct tnode *left;
    struct tnode *right;
};

struct tnode *talloc_(void)
{
    return (struct tnode *)malloc(sizeof(struct tnode));
}



char *strdup_(char *s)
{
    char *p;
    p = (char *)malloc(strlen(s) + 1);
    if (p != NULL)
        strcpy(p, s);
    return p;
}


struct tnode *addtree(struct tnode *p, char *w)
{
    int cond;
    if (p == NULL)
    {                 /* a new word has arrived */
        p = talloc_(); /* make a new node */
        p->word = strdup_(w);
        p->count = 1;
        p->left = p->right = NULL;
    }else if ((cond = strcmp(w, p->word)) == 0)
        p->count += 1;
    else if (cond < 0) /* less than into left subtree */
        p->left = addtree(p->left, w);
    else /* greater than into right subtree */
        p->right = addtree(p->right, w);
    return p;
}

int count_num = 0;
char *word_coll[MAXWORDCOUNT];
int count_coll[MAXWORDCOUNT];
void treeToArr(struct tnode *p_)
{
    if (p_ != NULL)
    {
        treeToArr(p_->left);
        word_coll[count_num] = p_->word;
        count_coll[count_num] = p_->count;
        ++count_num;
        treeToArr(p_->right);
    }
}

void swapInt(int *xp, int *yp){
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
void swapString(char **str1_ptr, char **str2_ptr) 
{ 
  char *temp = *str1_ptr; 
  *str1_ptr = *str2_ptr; 
  *str2_ptr = temp; 
}  
void bubbleSortArr(int count_coll[], char *word_coll, int n){
   int i, j;
    for (i = 0; i < n-1; i++)
        for (j = 0; j < n-i-1; j++)
            if (count_coll[j] > count_coll[j+1]){
                swapInt(&count_coll[j], &count_coll[j+1]);
                swapString(&word_coll[j], &word_coll[j+1]);
            }
}

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    struct tnode *root;
    char word[MAXWORDLEN];
    root = NULL;
    while (getword(word)==1)
        root = addtree(root, word);
    treeToArr(root);
    bubbleSortArr(count_coll,word_coll,count_num);
    for(int i=0; i<count_num; ++i){
        printf("%s ",word_coll[i]);
        printf("%d ",count_coll[i]);
        putchar('\n');
    }
    return 0;
}