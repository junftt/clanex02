#include <stdio.h>
#define is_leap(year) ((year) % 4 == 0 && (year) % 100 != 0 || (year) % 400 == 0)

static char daytab[2][13] = {
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
    {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};
/* day_of_year: set day of year from month & day */
int day_of_year(int year, int month, int day)
{
    if(month<1 || month>12){
        return -1;
    }
    int i, leap;//change to marco
    leap = is_leap(year);
    if(day<1 || day>daytab[leap][month]){
        return -1;
    }
    for (i = 1; i < month; i++)
        day += daytab[leap][i];
    return day;
}
/* month_day: set month, day from day of year */
void month_day(int year, int yearday, int *pmonth, int *pday)
{
    if(yearday<1){
        *pmonth = -1;
        *pday = -1;
        return;
    }
    int i, leap;
    leap = is_leap(year);
    if(leap){
        if(yearday>366){
            *pmonth = -1;
            *pday = -1;
            return;
        }
    }else{
        if(yearday>365){
            *pmonth = -1;
            *pday = -1;
            return;
        }
    }
    for (i = 1; yearday > daytab[leap][i]; i++)
        yearday -= daytab[leap][i];
    *pmonth = i;
    *pday = yearday;
}

int main(){
    printf("%d\n", day_of_year(2020,3,27));
    int pmonth = 2020;
    int pday = 127;
    month_day(2020,127,&pmonth,&pday);
    printf("%d%d\n", pmonth, pday);
    return 0;
}