/*input:
	asdas\asd
*/

#include <stdio.h>
#define MAX 100
int escape(char t[], char n[]);

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    char t[MAX], n[MAX];
    escape1(t,n);
    escape2(t,n);
    printf(t);
    putchar('\n');
    printf(n);
}

int escape1(char t[], char n[]){
    char c;
    int i = 0;
    while ((c=getchar())!=EOF){
        switch (c){
            case '\n':
                t[i++] = '\\';
                t[i++] = 'n';
                break;
            case '\t':
                t[i++] = '\\';
                t[i++] = 't';
                break;
            case '\0':
                t[i++] = '\\';
                t[i++] = '0';
                break;
            case '\\':
                t[i++] = '\\';
                t[i++] = '\\';
                break;
            default:
                t[i++] = c;
                break;
        }
    }
    t[i] = '\0';
}

int escape2(char t[], char n[]){
    int last_is_slash = 0;
    int index = 0;
    int i = 0;
    while(t[i]!=EOF){
        switch (t[i]){
            case '\\':
                if(last_is_slash){
                    n[index++]='\\';
                    last_is_slash = 0;
                }else{
                    last_is_slash = 1;
                }
                break;
            case 'n':
                if(last_is_slash){
                    n[index++]='\n';
                }else{
                    n[index++]='n';
                }
                last_is_slash = 0;
                break;
            case 't':
                if(last_is_slash){
                    n[index++]='\t';
                }else{
                    n[index++]='t';
                }
                last_is_slash = 0;
                break;
            case '0':
                if(last_is_slash){
                    n[index++]='\0';
                }else{
                    n[index++]='0';
                }
                last_is_slash = 0;
                break;
            default:
                n[index++]=t[i];
                break;
        }
        i++;
    }
}
