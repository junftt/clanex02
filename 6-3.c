#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#define MAXWORDLEN 20
#define MAXLINENUM 20
/*
play carti carti plug plug plug
play carti plug
play plug
plug

carti 1 2 
play 1 2 3 
plug 1 2 3 4 
*/
int line_index = 1;
char c = ' ';

int getword(char w[MAXWORDLEN])
{
    int index = 0;
    while (!isalpha(c)){
        if(c == EOF){
            return -1;
        }else if(c == '\n'){
            ++line_index;
        }
        c = getchar();
    }

    w[index++] = c;
    while (isalpha(c = getchar())){
        w[index++] = c;
    }
    w[index] = '\0';
    return 1;
}

struct tnode{
    char *word;
    int line_num[MAXLINENUM];
    int line_num_arr_index;
    struct tnode *left;
    struct tnode *right;
};

void addLineNum(int line_num[MAXLINENUM], int *line_num_arr_index){
    for(int i=0; i<=(*line_num_arr_index); ++i){
        if(line_num[i] == line_index){
           return; 
        }
    }
    line_num[(*line_num_arr_index)++] = line_index;
}

struct tnode *talloc_(void)
{
    return (struct tnode *)malloc(sizeof(struct tnode));
}



char *strdup_(char *s)
{
    char *p;
    p = (char *)malloc(strlen(s) + 1);
    if (p != NULL)
        strcpy(p, s);
    return p;
}


struct tnode *addtree(struct tnode *p, char *w)
{
    int cond;
    if (p == NULL)
    {                 /* a new word has arrived */
        p = talloc_(); /* make a new node */
        p->word = strdup_(w);
        p->line_num_arr_index = 0;
        addLineNum(p->line_num, &(p->line_num_arr_index));
        p->left = p->right = NULL;
    }else if ((cond = strcmp(w, p->word)) == 0)
        addLineNum(p->line_num, &(p->line_num_arr_index));
    else if (cond < 0) /* less than into left subtree */
        p->left = addtree(p->left, w);
    else /* greater than into right subtree */
        p->right = addtree(p->right, w);
    return p;
}

void treeprint(struct tnode *p)
{
    if (p != NULL)
    {
        treeprint(p->left);
        printf("%s ", p->word);
        for(int i = 0; i<p->line_num_arr_index; ++i){
            printf("%d ", (p->line_num)[i]);
        }
        putchar('\n');
        treeprint(p->right);
    }
}

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    struct tnode *root;
    char word[MAXWORDLEN];
    root = NULL;
    while (getword(word)==1)
        root = addtree(root, word);
    treeprint(root);
    return 0;
}