#include <stdio.h>
#include <ctype.h>
#define MAXCOL 10
#define MAXWORDLEN 20

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    char w[MAXWORDLEN];
    int word_index = 0;
    int col = 0;
    int word_num = 0;
    int last_is_letter = 0;

    char c;
    while((c=getchar())!=EOF){
        ++col;
        if(isalpha(c)){
            last_is_letter = 1;
            w[word_index++] = c;
        }else{
            if(last_is_letter){
                w[word_index] = '\0';
                word_index = 0;
                ++word_num;
                if(col<=MAXCOL){
                    printf("%s", w);
                }else{
                    if(word_num == 1){
                        printf("%s", w);
                    }else{
                        putchar('\n');
                        printf("%s", w);
                        col = 0;
                        word_num = 0;
                    }
                }
                if(c == '\n'){
                    putchar('\n');
                    col = 0;
                    word_num = 0;
                }else{
                    putchar(c);
                }
            }else{
                putchar(c);
                if(c == '\n'){
                    putchar('\n');
                    col = 0;
                    word_num = 0;
                }
            }
            last_is_letter = 0;
        }
    }
}