#include <ctype.h>
#include <stdio.h>
#include <string.h>

double atof(char s[])
{
    double val, power;
    double sign;
    int i;
    for (i = 0; isspace(s[i]); i++) /* skip white space */
        ;
    sign = (s[i] == '-') ? -1.0 : 1.0;
    if (s[i] == '+' || s[i] == '-')
        i++;
    for (val = 0.0; isdigit(s[i]); i++)
        val = 10.0 * val + (s[i] - '0');
    if (s[i] == '.')
        i++;
    for (power = 1.0; isdigit(s[i]); i++)
    {
        val = 10.0 * val + (s[i] - '0');
        power *= 10;
    }
    double res = sign * val / power;
    //check sign == 'e'
    while(isalpha(s[i])){
        if(s[i]!='e'){
            printf("%s","wrong format");
            return -1;
        }
        ++i;
    }
    sign = (s[i++] == '-') ? -1.0 : 1.0;
    while(s[i]!='\0'){
        if(!isdigit(s[i])){
            printf("%s","wrong format");
            return -1;
        }
        ++i;
    }
    i-=1;
    val = 0;
    for (power = 1.0; isdigit(s[i]); i--)//参考第14行
    {
        val = power*(s[i] - '0')+val;
        power *= 10;
    }
    if(sign>0){
        for (int i = 0; i < val; i++){
            res*=10.0;
        }
    }else{
        for (int i = 0; i < val; i++){
            res*=0.1;
        }
    }
    return res;
}

int main(){
    printf("%g\n", atof("13.03e10"));
    printf("%g\n", atof("123.45e-6"));
    /*
        130299999999.999980
        0.000123
    */
    return 0;
}