#include <stdio.h>
#define MAX_LEN 100

void reverse(char s[], int len){
    if(s[len] == '\n' || s[len] == '\0'){
        --len;
    }
    char temp;
    int i = 0, j = len;
    while(i<j){
        temp = s[i];
        s[i] = s[j];
        s[j] = temp;
        ++i;
        --j;
    }
}

void itoa(int n, char s[], int min)
{
    int i = 0, sign;
    if ((sign = n) < 0){
        s[i++] = -(n % 10) + '0';
        n = -(n/10);// 最大负数：1000000，如果直接n=-n，就会变成0000000
    } /* record sign */
    
    do
    {                          /* generate digits in reverse order */
        s[i++] = n % 10 + '0'; /* get next digit */
    } while ((n /= 10) > 0);   /* delete it */
    if (sign < 0)
        s[i++] = '-';
    while(i<min){
        s[i++] = ' ';
    }
    s[i] = '\0';
    reverse(s, i);
}

int main(){
    char s[MAX_LEN];
    //itoa(1171233,s,10);
    //printf(s);
    //putchar('\n');
    itoa(-11789733,s,10);
    printf(s);
}