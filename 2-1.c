#include <stdio.h>
#include <limits.h>
#include <math.h>
#define ACCURACY 10
int findMaxint(signed int a){
    signed int b = a;
    while(++a>b){
        b = a;
    }
    return b;
}
int findMinint(signed int a){
    signed int b = a;
    while(--a<b){
        b = a;
    }
    return b;
}
float findMaxfloat(){
    float a = 1;
    int power = 0;
    while(isfinite(a)){
        ++power;
        a*=10;
    }
    a=pow(10,--power);

    int bit = 1;
    while(isfinite(a*bit)){
        ++bit;
    }--bit;
    a*=bit;
    for(int i=1; i<=ACCURACY; ++i){
        --power;
        for(int j=1; j<=10; ++j){
            float b = a+(pow(10,power)*j);
            if(isinf(b)){
                a = a+pow(10,power)*(j-1);
                break;
            }
        }
    }
    return a;
}
int main(){
    printf("signed char min:%d\n",SCHAR_MIN);
    printf("signed char max:%d\n",SCHAR_MAX);
    printf("signed short min:%d\n",SHRT_MIN);
    printf("signed short max:%d\n",SHRT_MAX);
    printf("signed int min:%d\n",INT_MIN);
    printf("signed int max:%d\n",INT_MAX);
    printf("signed long min:%ld\n",LONG_MIN);
    printf("signed long max:%ld\n",LONG_MAX);

    printf("unsigned char max:%u\n",UCHAR_MAX);//%u is for unsigned interger
    printf("unsigned short max:%u\n",USHRT_MAX);
    printf("unsigned int max:%u\n",UINT_MAX);
    printf("unsigned long max:%lu\n",ULONG_MAX);

    //printf("%d", findMaxint(0));
    //printf("%d", findMinint(0));
    //2147483647 -2147483648

    float a;
    a=findMaxfloat();
    printf("%f",a);
}