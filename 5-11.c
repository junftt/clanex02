#include <stdio.h>
#include <stdlib.h>
#define TABS_INTERVAL 4
#define STATION_SIZE 100

int stops_station[STATION_SIZE];//first element: the next stops' index

int getNextStop_(){
    if(stops_station[stops_station[0]]!=-1){
        return stops_station[stops_station[0]++];
    }else{
        return stops_station[stops_station[0]++] = stops_station[stops_station[0]-1]+TABS_INTERVAL;
    }
}
int getNextStop(int curPos, int low_blank_position, int* previousStop){
    int stop_position = getNextStop_();
    int proper_stops_num = 0;
    int real_output_blank = -1;
    while(stop_position<=curPos){
        if(stop_position>=low_blank_position && stop_position<=curPos){
            ++proper_stops_num;
            real_output_blank = curPos-stop_position;
            previousStop = stop_position;
        }
        stop_position = getNextStop_();
    }
    --stops_station[0];
    return real_output_blank;
}
void entab(){
    char c;
    int line_index = -1;
    int last_is_alpha = 0;
    int last_is_blank = 0;
    int low_blank_position = -1;
    int high_blank_position = -1;
    while ((c = getchar())!=EOF){
        if(c == ' '){
            if(last_is_alpha){
                low_blank_position = line_index;
            }
            ++line_index;
            last_is_blank = 1;
            last_is_alpha = 0;
        }else if(c == '\n'){
            line_index = 0;
            stops_station[0] = 1;
            putchar(c);
        }else{
            if(last_is_blank){
                high_blank_position = line_index;
                int stop_position = getNextStop_();
                int proper_stops_num = 0;
                int real_output_blank = -1;
                while(stop_position<=line_index){
                    if(stop_position>=low_blank_position && stop_position<=high_blank_position){
                        ++proper_stops_num;
                        real_output_blank = high_blank_position-stop_position;
                    }
                    stop_position = getNextStop_();
                }
                goBackStop();
                for(int i=0;i<proper_stops_num;++i){
                    printf("%s", "\\t");
                }
                for(int j=0;j<real_output_blank;++j){
                    putchar(' ');
                }
                low_blank_position = -1;
                high_blank_position = -1;
            }
            ++line_index;
            putchar(c);
            last_is_alpha = 1;
            last_is_blank = 0;
        }
    }
}

void detab(){
    char c;
    int line_index = 0;
    while ((c = getchar())!=EOF){
        if(c == '\t'){
            int stop_position;
            do{
                stop_position = getNextStop_();
            }while(stop_position<=line_index);
            for(;line_index<=stop_position;++line_index){
                putchar(' ');
            }
        }else if(c == '\n'){
            line_index = 0;
            stops_station[0] = 1;
            putchar(c);
        }else{
            ++line_index;
            putchar(c);
        }
    }
}

int main(int argc, char *argv[]){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);


    for(int i=0; i<STATION_SIZE; ++i){
        stops_station[i] = -1;
    }
    stops_station[0] = 1;
    
    for(int i=1; i<argc; ++i){
        stops_station[i] = atoi(argv[i]);
    }
    stops_station[1] = 3;
    stops_station[2] = 8;
    stops_station[3] = 13;
    stops_station[4] = 15;
    entab();

    return 0;
}