#include <stdio.h>
//printBits？？？？？
int setbits(int x, int p, int n, int y);
void printBits(int x);

int main(){
    printf("%d", setbits(11597, 6, 4, 455));
    return 0;
}

int setbits(int x, int p, int n, int y){
    //x: 10110 1010 01101 11597
    //y: 11100 0111 455
    //p: 6
    //n: 4
    int z = 0;
    z = ~(((1<<n) -1) << (p-1));//11111 0000 11111
    printBits(z);
    x = x & z;//10110 0000 01101
    y = y << (p-1);//11100 0111 00000
    y = y & (~z);//00000 0111 00000
    printBits(y);
    x = x | y;//10110 0111 01101
    printBits(x);
    return x;
}

void printBits(int x) {
	int len = sizeof(x)*8;
    int temp;
    for(;len>0;--len){
        temp = (x&1);
        printf("%d",temp);
        x = x >> 1;
    }
    putchar('\n');
}
