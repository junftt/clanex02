#include <stdio.h>
int rightrot(int x, int n);

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    printf("%d\n", rightrot(605, 3));
}

void printBits(int x) {
	int len = sizeof(x)*8;
    int temp;
    for(;len>0;--len){
        temp = (x&1);
        printf("%d",temp);
        x = x >> 1;
    }
    putchar('\n');
}

int rightrot(int x, int n){
    int z = 0;
    z = (1<<n) -1;
    int temp = (x&z)<<(sizeof(x)*8-n);
    x = x>>n;
    x = x|temp;
    printBits(x);
}