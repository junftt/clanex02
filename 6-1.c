#include <stdio.h>
#define MAX 20

int getword(char *word, int lim)
{
    int c;
    char *w = word;
    while (isspace(c = getch()))
        ;
    if (c != EOF)
        *w++ = c;
    if (!isalpha(c)) {
        *w = '\0';
        return c;
    }
    for ( ; --lim > 0; w++)
        if (!isalnum(*w = getch())) {
            ungetch(*w);
            break;
        }
    *w = '\0';
    return word[0];
}

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    char *w;
    while(getword(w, MAX)){
        printf("%s\n", w);
    }
    return 0;
}