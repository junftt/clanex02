#include <stdio.h>
#define TABS 4

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    char c;
    int len_of_line = 0;
    int len_of_blank = 0;
    int last_is_blank = 0;
    while ((c = getchar())!=EOF){
        if(c == ' '){
            ++len_of_blank;
            ++len_of_line;
            last_is_blank = 1;
        }else{
            if(last_is_blank){
                last_is_blank = 0;
                int i = len_of_line%TABS;
                if(len_of_blank < i){
                    for(int j=0; j<len_of_blank; ++j){
                        putchar(' ');
                    }
                }else{
                    len_of_blank=len_of_blank-i;
                    while ((len_of_blank-TABS)>0)
                    {
                        len_of_blank=len_of_blank-TABS;
                        putchar('\t');
                    }
                    putchar('\t');
                    for(; i>0; --i){
                        putchar(' ');
                    }
                }
                len_of_blank=0;
            }
            if(c == '\n'){
                len_of_line = 0;
                len_of_blank = 0;
                last_is_blank = 0;
                putchar(c);
            }else{
                ++len_of_line;
                putchar(c);
            }
        }
    }
    
    return 0;
}