#include <stdio.h>
//长度不包括'\0'
int strindex(char s[], int len_s, char t[], int len_t){
	//strlen
	for(int i = len_s-1; i>=0; --i){
		int index_of_s = i;
		int j = len_t-1;
		for(; j>=0; --j){
			if(s[index_of_s--]!=t[j]){
				break;
			}
		}
		if(j == -1){
			return i-(len_t-1);
		}
	}
	return -1;
}

int main(){
	printf("%d", strindex("abcdttttt",9,"ttt",3));
	//6
	printf("%d", strindex("abcdttttt",9,"aat",3));
	//-1
	printf("%d", strindex("abcdttttt",9,"cdt",3));
	//2
	return 0;
}