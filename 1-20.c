#include <stdio.h>
#define TABS 4

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    char c;
    int len_of_line = 0;
    while ((c = getchar())!=EOF){
        if(c == '\t'){
            while(++len_of_line%TABS!=0){
                putchar(' ');
            }
            putchar(' ');
        }else if(c == '\n'){
            len_of_line = 0;
            putchar(c);
        }else{
            ++len_of_line;
            putchar(c);
        }
    }
    
    return 0;
}