#include <stdio.h>
#include <stdlib.h>

int* runningSum(int* nums, int numsSize){
    int* p;
    p = (int *) malloc(sizeof(int)*numsSize);
    *p = *nums;
    for(int i=1; i<numsSize; ++i){
        *(p+i)=*(nums+i)+*(p+i-1);
    }
    return p;
}

int main(){
    int num[5] = {1,2,3,4,5};
    runningSum(num, 5);
    return 1;
}

