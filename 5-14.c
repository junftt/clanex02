#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define OFF 0
#define ON 1
#define VSIZE 4
int r_opt = OFF;

void swap(void *v[], int i, int j)
{
    void *temp;
    temp = v[i];
    v[i] = v[j];
    v[j] = temp;
}
/* numcmp: compare s1 and s2 numerically */
int numcmp(char *s1, char *s2)
{
    double v1, v2;
    v1 = atof(s1);
    v2 = atof(s2);
    if (v1 < v2)
        return -1;
    else if (v1 > v2)
        return 1;
    else
        return 0;
}
/* qsort_: sort v[left]...v[right] into increasing order */
void qsort_(void *v[], int left, int right,
           int (*comp)(void *, void *))
{
    int i, last;
    void swap(void *v[], int, int);
    if (left >= right) /* do nothing if array contains */
        return;        /* fewer than two elements */
    swap(v, left, (left + right) / 2);
    last = left;
    for (i = left + 1; i <= right; i++){
        if ((*comp)(v[i], v[left]) < 0 && r_opt==0){
            swap(v, ++last, i);
        }else if((*comp)(v[i], v[left]) > 0 && r_opt==1){
            swap(v, ++last, i);
        }
    }       
    swap(v, left, last);
    qsort_(v, left, last - 1, comp);
    qsort_(v, last + 1, right, comp);
}

int main(int argc, char *argv[]){
    char *v[VSIZE] = {"3","9","6","1"};
    int n_opt = OFF;

    for(int i=1; i<argc; ++i){
        if(strcmp(argv[i], "-n")==0){
            n_opt = ON;
        }else if(strcmp(argv[i], "-r")==0){
            r_opt = ON;
        }
    }
    if(n_opt){
        qsort_(v,0,VSIZE-1,numcmp);
    }else{
        qsort_(v,0,VSIZE-1,strcmp);
    }
    for(int i=0; i<VSIZE; ++i){
        printf("%s\n", v[i]);
    }
    return 0;
}
