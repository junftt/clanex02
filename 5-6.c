#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAXLEN 100
#define NUMBER '0'


void reverse_(char *s){//passed
    char *m = s+strlen(s)-1;
    for(;s<m;++s,--m){
        char c = *s;
        *s = *m;
        *m = c;
    }
}

void getline_(char *s){
    char c;
    while((c = getchar())!='\n' && c!=EOF){
        *s++ = c;
    }
    *s = c;
}

int atoi_(char *s){
    char *s_0 = s;
    int num = 0;
    while (*s++ != '\0'){
        ;
    }
    s-=2;

    int i = 0;
    for(;(s+1) != s_0; --s,++i){
        num+=((*s-'0')*pow(10,i));
    }
    return num;
}

void itoa_(int n, char *s){
    int i, sign;
    if ((sign = n) < 0)
        n = -n;
    do{                         
        *s++ = n % 10 + '0';
    } while ((n /= 10) > 0);
    if (sign < 0)
        *s++ = '-';
    *s = '\0';
    reverse_(s);
}

int strindex(char *s, char *t)
{
    char *ini = s;
    char *p1;
    char *p2;
    for (; *s != '\0'; s++){
        for (p1 = s, p2 = t; *p2 != '\0' && *p1 == *p2; p1++, p2++);
        if (p2 > t && *p2 == '\0')
            return s-ini;
    }
    return -1;
}


int main(){
    char s[100] = "123";
    int a = atoi_(s);
    printf("%d",a);
    return 1;
}