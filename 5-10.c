#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h>
#define MAXOP 100
#define NUMBER '0'
#define MAXVAL 100 
#define BUFSIZE 100
//debug needed?????
int sp = 0;
double val[MAXVAL];

void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f;
    else
        printf("error: stack full, can't push %g\n", f);
}

double pop(void)
{
    if (sp > 0)
        return val[--sp];
    else
    {
        printf("error: stack empty\n");
        return 0.0;
    }
}
char buf[BUFSIZE]; /* buffer for ungetch */
int size = 0;
char *cur = buf;
char *stop = buf;
char *ini_pos = buf;
char *end_pos = buf+(BUFSIZE-1);
int getch(void)    /* get a (possibly pushed-back) character */
{
    int c;
    if(size>0){
        if(cur == end_pos){
            c = *cur;
            cur = ini_pos;
        }else{
            c = *cur++;
        }
        --size;
        return c;
    }else{
        return EOF;
    }    
}
void ungetch(int c) /* push character back on input */
{
    if(size>=BUFSIZE){
        printf("ungetch: too many characters\n");
    }else{
        if(stop == end_pos){
            *stop = c;
            stop = ini_pos;
        }else{
            *stop++ = c;
        }
        ++size;
    }
}
void ungets(char* s){
    while(*s != '\0'){
        ungetch(*s++);
    }
}

int getop(char s[])
{
    int i, c;
    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c; /* not a number */
    i = 0;
    if (isdigit(c)) /* collect integer part */
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.') /* collect fraction part */
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);
    return NUMBER;
}

int main(int argc, char *argv[])
{
    int type;
    double op2;
    char s[MAXOP];

    while (--argc>0)
    {
        ungets(" ");
        ungets(*++argv);
        switch (getop(s))
        {
        case NUMBER:
            push(atof(s));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '*':
            push(pop() * pop());
            break;
        case '-':
            op2 = pop();
            push(pop() - op2);
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2);
            else
                printf("error: zero divisor\n");
            break;
        default:
            printf("error: unknown command %s\n", s);
            break;
        }
        
    }
    printf("\t%.8g\n", pop());
    getchar();
    return 0;
}