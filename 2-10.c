#include <stdio.h>
#define MAX 100

void lower(char s[]);

int main(){
    char s[] = "AcBjKdLSK";
    lower(s);
    printf("%s", s);
}

void lower(char s[]){
    int i = 0;
    while(s[i]!='\0'){
        s[i] = ((s[i]>='A'&&s[i]<='Z')?s[i]+32:s[i]);
        ++i;
    }
}
