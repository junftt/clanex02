#define BUFSIZE 100
char line[BUFSIZE]; /* buffer for ungetch */
int index = 0;      /* next free position in buf */
int getch()    /* get a (possibly pushed-back) character */
{
    return line[index++];
}
void ungetch(int c) /* push character back on input */
{
    if(index>0){
        --index;
    }
}
