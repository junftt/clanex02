#include <stdio.h>
int itoa2_recursive(int a, char* b, int index) {
	int cnt = 0;
	if (a>=10) {
		cnt = itoa2_recursive(a/10, b, index);
 	}
     
	b[index+cnt] = '0' + a%10;
	return cnt+1;
}

void itoa(int a, char* b) {
	int index = itoa2_recursive(a,b,0);
	b[index] = '\0';
}

int main(){
    int a = 12345;
    char b[32];
    itoa(a, b);
	printf("%s", b);
	return 0;
}