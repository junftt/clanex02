#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <stdlib.h> /* for atof() */
#define MAXOP 100   /* max size of operand or operator */
#define NUMBER '0'  /* signal that a number was found */
#define MAXVAL 100 
#define BUFSIZE 100
#define SIN 's'
#define EXP 'e'
#define POW 'p'
/*
30 s 30 +
2 e 10 +
10 2 p

	29.011968
	17.389056
	100
*/


int sp = 0;         /* next free stack position */
double val[MAXVAL]; /* value stack */
/* push: push f onto value stack */
void push(double f)
{
    if (sp < MAXVAL)
        val[sp++] = f;
    else
        printf("error: stack full, can't push %g\n", f);
}
/* pop: pop and return top value from stack */
double pop(void)
{
    if (sp > 0)
        return val[--sp];
    else
    {
        printf("error: stack empty\n");
        return 0.0;
    }
}

char buf[BUFSIZE]; /* buffer for ungetch */
int size = 0;
char *cur = buf;
char *stop = buf;
char *ini_pos = buf;
char *end_pos = &buf[BUFSIZE];
int getch(void)    /* get a (possibly pushed-back) character */
{
    int c;
    if(size>0){
        if(cur == end_pos){
            c = *cur;
            cur = ini_pos;
        }else{
            c = *cur++;
        }
        --size;
        return c;
    }else{
        return getchar();
    }    
}
void ungetch(int c) /* push character back on input */
{
    if(size>=BUFSIZE){
        printf("ungetch: too many characters\n");
    }else{
        if(stop == end_pos){
            *stop = c;
            stop = ini_pos;
        }else{
            *stop++ = c;
        }
        ++size;
    }
}

void ungets(char* s){
    while(*s != '\0'){
        ungetch(*s++);
    }
}

/* getop: get next character or numeric operand */
int getop(char s[])
{
    int i, c;
    while ((s[0] = c = getch()) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c; /* not a number */
    i = 0;
    if (isdigit(c)) /* collect integer part */
        while (isdigit(s[++i] = c = getch()))
            ;
    if (c == '.') /* collect fraction part */
        while (isdigit(s[++i] = c = getch()))
            ;
    s[i] = '\0';
    if (c != EOF)
        ungetch(c);
    return NUMBER;
}

//直接用c语言的取余函数
int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    int type;
    double op2;
    char s[MAXOP];
    while ((type = getop(s)) != EOF)
    {
        switch (type)
        {
        case NUMBER:
            push(atof(s));
            break;
        case '+':
            push(pop() + pop());
            break;
        case '*':
            push(pop() * pop());
            break;
        case '-':
            op2 = pop();
            push(pop() - op2);
            break;
        case '/':
            op2 = pop();
            if (op2 != 0.0)
                push(pop() / op2);
            else
                printf("error: zero divisor\n");
            break;
        case SIN:{
            int temp = pop();
            push(sin(temp));
            break;
        }
        case EXP:{
            int temp = pop();
            push(exp(temp));
            break;
        }
        case POW:{
            int temp = pop();
            push(pow(pop(),temp));
            break;
        }
        case '\n':
            printf("\t%.8g\n", pop());
            break;
        default:
            printf("error: unknown command %s\n", s);
            break;
        }
    }
    return 0;
}