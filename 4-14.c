#include <stdio.h>
#define swap(type, y, z) {  type temp;\
                            temp = z;\
                            z = y;\
                            y = temp;\
                         }

int main(){
    int a = 1;
    int b = 2;
    swap(int, a, b);
    printf("%d\n", a);
    printf("%d\n", b);
    return 0;
}