#include <stdio.h>
/*
ad    dgddg
sdaf


ac
*/
int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    int cr;
    int last = 1;//0 - the last char is not a letter, 1 - the last char is a letter
    while ((cr=getchar())!=EOF)
    {
        if(cr==' ' || cr=='\n' || cr=='\t'){
            if(last == 1){
                printf("%c",'\n');
                last = 0;
            }
        }else{
            putchar(cr);
            last = 1;
        }
    }
    return 0;
}
