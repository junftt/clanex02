#include <stdio.h>
int invertBits(int x, int p, int n);

int main(){
    printf("%d", invertBits(597, 3, 3));
}

int invertBits(int x, int p, int n){
    //x: 10010 101 01 597
    //p: 3
    //n: 3
    int z = 0;
    int pn;
    z = ((1<<n) -1) << (p-1);
    x = x^z;
    return x;
}