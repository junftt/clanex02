#include <stdio.h>

void printBits(int x) {
	int len = sizeof(x)*8;
    int temp;
    for(;len>0;--len){
        temp = (x&1);
        printf("%d",temp);
        x = x >> 1;
    }
    putchar('\n');
}

int bitcountUnsigned(unsigned int x){
    int b = 0;
    while(x!=0){
        ++b;
        x &= (x-1);
    }
    return b;
}

int bitcountSigned(signed int x){
    bitcountUnsigned(*((unsigned int*)(&x)));
}

int main(){
    printBits(597);
    printBits(-597);
    printf("%d\n", bitcountUnsigned(597));
    printf("%d\n", bitcountSigned(-597));
}