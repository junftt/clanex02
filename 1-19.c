#include <stdio.h>
#define MAXLINE 1000 /* maximum input line length */

int readLine(char line[], int maxline);
void reverse(char s[], int len);
/* print the longest input line */
int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    int len;               /* current line length */
    int max;               /* maximum length seen so far */
    char line[MAXLINE];    /* current input line */
    char longest[MAXLINE]; /* longest line saved here */
    max = 0;
    while ((len = readLine(line, MAXLINE)) > 0){//
        reverse(line,len);
        printf("%s",line);
    }//
    return 0;
}
/* readLine: read a line into s, return length */
int readLine(char s[], int lim)
{
    int c, i;
    for (i = 0; i < lim - 1 && (c = getchar()) != EOF && c != '\n'; ++i)
        s[i] = c;
    if (c == '\n')
    {
        s[i] = c;
        ++i;
    }
    s[i] = '\0';
    return i;
}

void reverse(char s[], int len){
    if(s[len] == '\n' || s[len] == '\0'){
        --len;
    }
    char temp;
    int i = 0, j = len;
    while(i<j){
        temp = s[i];
        s[i] = s[j];
        s[j] = temp;
        ++i;
        --j;
    }
}