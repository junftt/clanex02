#define BUFSIZE 1
char buf = '\0';
int getch()    /* get a (possibly pushed-back) character */
{
    if(buf!='\0'){
        int temp = buf;
        buf = '\0';
        return temp;
    }else{
        return getchar();
    }
}
void ungetch(int c) /* push character back on input */
{
    if(buf=='\0'){
        buf = c;
    }
}