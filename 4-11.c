#include <ctype.h>
#include <stdio.h>
#define NUMBER '0' /* signal that a number was found */
#define BUFSIZE 100

int getop(char s[])
{
    static char buf[BUFSIZE]; /* buffer for ungetch */
    static int bufp = 0; /* next free position in buf */
    
    int i, c;
    while ((s[0] = c =  ((bufp>0) ? buf[--bufp] : getchar())) == ' ' || c == '\t')
        ;
    s[1] = '\0';
    if (!isdigit(c) && c != '.')
        return c; /* not a number */
    i = 0;
    if (isdigit(c)) /* collect integer part */
        while (isdigit(s[++i] = c = ((bufp>0) ? buf[--bufp] : getchar())));
    if (c == '.') /* collect fraction part */
        while (isdigit(s[++i] = c = ((bufp>0) ? buf[--bufp] : getchar())))
            ;
    s[i] = '\0';
    if (c != EOF){
        if (bufp >= BUFSIZE)
            printf("ungetch: too many characters\n");
        else
            buf[bufp++] = c;
    }
        ungetch(c);
    return NUMBER;
}
