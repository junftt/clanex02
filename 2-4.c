//output:jksdlsk
#include <stdio.h>

void sque(char s1[], char s2[]);

int main(){
    char s1[] = "acbjksdbblcsk";
    char s2[] = "abc";
    sque(s1, s2);
    printf("%s", s1);
}

void sque(char s1[], char s2[]){
    int k = 0;

    for(int i = 0; s1[i]!='\0'; ++i){
        int j = 0;
        for(; s2[j]!='\0'; ++j){
            if(s1[i]==s2[j]){
                break;
            }
        }
        if(s2[j]=='\0'){
            s1[k++] = s1[i];
        }
    }
    s1[k] = '\0';
}