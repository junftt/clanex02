#include <stdio.h>
#include <string.h>

int htoi(char s[]){
    if(!(strlen(s)>2 && s[0] == '0' && (s[1] == 'x' || s[1] == 'X'))){
        return 0;
    }
    
    int i = 2;
    int keep_on = 1;
    int num = 0;
    int point;
    while(keep_on){
        if(s[i]>='0' && s[i]<='9'){
            point = s[i]-'0';
        }else if(s[i]>='a' && s[i]<='f'){
            point = s[i]-'a'+10;
        }else if(s[i]>='A' && s[i]<='F'){
            point = s[i]-'A'+10;
        }else{
            break;
        }
        num = 16*num+point;
        ++i;
    }
    return num;
}

int main(){
    int a = htoi("0xbc4ff2");
    printf("%d", a);
    return 1;
}