#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define MAX_LEN 100

void reverse(char s[], int len){
    if(s[len] == '\n' || s[len] == '\0'){
        --len;
    }
    char temp;
    int i = 0, j = len;
    while(i<j){
        temp = s[i];
        s[i] = s[j];
        s[j] = temp;
        ++i;
        --j;
    }
}

int atoi_(char c){
    if(isdigit(c)){
        return c-'0';
    }else if(isalpha(c)){
        return c-'a'+10;
    }
}

char carry_(char c){
    if(c=='9'){
        return 'a';
    }else{
        return c+1;
    }
}

void itob(int n, char s[MAX_LEN], int b){
    int carry = 1;
    int len = 0;
    for(int i = 0; i<n; ++i){
        int index = 0;
        while(carry){
            if(s[index]=='\0'){
                s[index] = '1';
                carry = 0;
            }else{
                if(atoi_(s[index])>=(b-1)){
                    s[index] = '0';
                    ++index;
                    if(index>len){
                        len = index;
                    }
                    carry = 1;
                }else{
                    s[index] = carry_(s[index]);
                    carry = 0;
                }
            }
        }
        carry = 1;
    }
    reverse(s, len);
}

int main(){
    char s[MAX_LEN];
    for(int i=0;i<MAX_LEN;++i){
        s[i] = '\0';
    }
    itob(13999,s,32);
    printf("%s",s);
    return 1;
}