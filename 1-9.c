#include <stdio.h>

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    int cr;
    int last = 0;//0 - the last char is not ' ', 1 - the last char is ' '
    while ((cr=getchar())!=EOF){
        if(cr==' '){
            if(last == 0){
                putchar(cr);
            }
            last = 1;
        }else{
            putchar(cr);
            last = 0;
        }
    }
    return 0;
}
