#include <stdio.h>
#include <ctype.h>
#define BUFSIZE 100


/* getfloat: get next integer from input into *pn */
int getfloat(float *pn)
{
    int c, sign;
    int p = 1;
    while (isspace(c = getchar())) /* skip white space */
        ;
    if (!isdigit(c) && c != EOF && c != '+' && c != '-')
    {
        return 0;
    }
    sign = (c == '-') ? -1 : 1;
    if (c == '+' || c == '-')
        c = getchar();
    for (*pn = 0.0; isdigit(c); c = getchar())
        *pn = 10 * (*pn) + (c - '0');
    if(c == '.'){
        c = getchar();
    }
    for(;isdigit(c);c=getchar()){
        *pn = 10 * (*pn) + (c - '0');
        p *= 10;
    }
    *pn *= sign;
    *pn /= p;
    return c;
}

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    float a = 0.0;
    float *pn = &a;
    getfloat(pn);
    printf("%f",*pn);
    return 1;
}