#include <stdio.h>

int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    
    int cr;
    while ((cr = getchar())!=EOF)
    {
        if(cr == '\t'){
            putchar('\\');
            putchar('t');
        }else if(cr == '\b'){
            putchar('\\');
            putchar('b');
        }else if(cr == '\\'){
            putchar('\\');
            putchar('\\');
        }else if('\n' == cr){//
            putchar('\\');
            putchar('n');
        }else{
            putchar(cr);
        }
        
    }
    return 0;
}
