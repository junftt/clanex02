#include <stdio.h>
#include <stdlib.h>
int TABS_INTERVAL = 4;
#define STATION_SIZE 100

int stops_station[STATION_SIZE];//first element: the next stops' index

int getNextStop(){
    if(stops_station[stops_station[0]]!=-1){
        return stops_station[stops_station[0]++];
    }else{
        return stops_station[stops_station[0]++] = stops_station[stops_station[0]-1]+TABS_INTERVAL;
    }
}
void goBackStop(){
    --stops_station[0];
}

void entab(){
    char c;
    int line_index = -1;
    int last_is_alpha = 0;
    int last_is_blank = 0;
    int low_blank_position = -1;
    int high_blank_position = -1;
    while ((c = getchar())!=EOF){
        if(c == ' '){
            if(last_is_alpha){
                low_blank_position = line_index;
            }
            ++line_index;
            last_is_blank = 1;
            last_is_alpha = 0;
        }else if(c == '\n'){
            line_index = 0;
            stops_station[0] = 1;
            putchar(c);
        }else{
            if(last_is_blank){
                high_blank_position = line_index;
                int stop_position = getNextStop();
                int closest_stop;
                int proper_stops_num = 0;
                int real_output_blank = -1;
                while(stop_position<=line_index){
                    if(stop_position>=low_blank_position && stop_position<=high_blank_position){
                        ++proper_stops_num;
                        real_output_blank = high_blank_position-stop_position;
                    }
                    stop_position = getNextStop();
                }
                goBackStop();
                for(int i=0;i<proper_stops_num;++i){
                    printf("%s", "\\t");
                }
                for(int j=0;j<real_output_blank;++j){
                    putchar(' ');
                }
                low_blank_position = -1;
                high_blank_position = -1;
            }
            ++line_index;
            putchar(c);
            last_is_alpha = 1;
            last_is_blank = 0;
        }
    }
}

void detab(){
    char c;
    int line_index = 0;
    while ((c = getchar())!=EOF){
        if(c == '\t'){
            int stop_position = getNextStop();
            while(stop_position<=line_index){
                stop_position = getNextStop();
            }
            for(;line_index<=stop_position;++line_index){
                putchar(' ');
            }
        }else if(c == '\n'){
            line_index = 0;
            stops_station[0] = 1;
            putchar(c);
        }else{
            ++line_index;
            putchar(c);
        }
    }
}

int atoi_(char *s){
    int i = 0;
    int n = 0;
    while(s[i]!='\0'){
        if(s[i] >= '0' && s[i] <= '9'){
            n = 10 * n + (s[i] - '0');
        }
        ++i;
    }
    return n;
}

int main(int argc, char *argv[]){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);


    for(int i=0; i<STATION_SIZE; ++i){
        stops_station[i] = -1;
    }
    stops_station[0] = 1;

    //stops_station[1] = atoi_(argv[1]);
    //TABS_INTERVAL = atoi_(argv[2]);
    stops_station[1] = 5;
    TABS_INTERVAL = 3;
    detab();

    return 0;
}