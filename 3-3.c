#include <stdio.h>

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);

    char c;
    int last_is_line = 0;
    char last_noneline;
    int len = 0;
    while ((c=getchar())!=EOF){
        if(c=='-'){
            last_is_line = 1;
        }else{
            if(last_is_line){
                last_is_line = 0;
                for(int i = last_noneline+1; i<=c; ++i){
                    putchar(i);
                }
            }else{
                putchar(c);
            }
            last_noneline = c;
        }
    }
}