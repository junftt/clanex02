#include <stdio.h>
#define MAXLINE 1000

int getLine80(char s[]);
int main()
{
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    char line[MAXLINE];
    getLine80(line);
    return 0;
}

int getLine80(char s[]){
    int index = 0;
    char cr;
    int status = 0;
    while ((cr=getchar())!=EOF)
    {
        if(index>80){
            if(status==0){
                printf("%s", s);
                status = 1;
            }
            putchar(cr);    
        }else{
            s[index++] = cr;
        }
        if(cr=='\n'){
            index = 0;
            status = 0;
        }
    }   
}