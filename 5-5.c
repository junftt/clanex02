#include <stdio.h>
#include <string.h>

void strncpy2(char *s, char *t, int n){
    while(n-->0 && *t){
        *s++ = *t++;
    }
   *s = '\0';
}

void strncat2(char *s, char *t, int n){
    while(*s++){
        ;
    }
    --s;
    strncpy2(s,t,n);
}

int strncmp2(char *s, char *t, int n){//找第一个不一样的字符，return c1-c2
    while(*s!='\0' && *t!='\0' && n>0){
        if(*s!=*t){
            break;
        }
        ++s;
        ++t;
        --n;
    }
    if((*s-*t)>0){
        return 1;
    }else if((*s-*t)<0){
        return -1;
    }else if((*s-*t)==0){
        return 0;
    }
    
}

int main(int argc, char *argv[]){
    char s[100];
    char t[100] = "efg";
    strncpy2(s, t, 5);
    printf("%s\n", s);

    strcpy(s, "abcd");
    strcpy(t, "efg");
    strncat2(s, t, 5);
    printf("%s\n", s);

    strcpy(s, "abcd");
    strcpy(t, "abc");
    printf("%d\n", strncmp2(s, t, 5));
}