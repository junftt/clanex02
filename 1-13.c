#include <stdio.h>
#define MAX 100

int checkNumLen(int nonezero){
    int i = 1;
    while(nonezero/=10){
        ++i;
    }
    return i;
}

void printHorizontalHistogram(int len_arr[], int max){
    int td_len_arr[MAX];
    for(int i=0; i<MAX; ++i){
        td_len_arr[i] = 0;
    }
    int num_of_nonzero = 0;
    for(int i=0, j=-1; i<MAX; ++i){
        if(len_arr[i] != 0){
            ++num_of_nonzero;
            td_len_arr[++j]=i;
            td_len_arr[++j]=len_arr[i];
        }
    }
    for(int i=1; i<=max; ++i){//多位对齐
        for(int j=0; j<num_of_nonzero; ++j){
            if((max-i)<td_len_arr[2*j+1]){
                putchar('*');
            }else{
                putchar(' ');
            }
            int numlen = checkNumLen(td_len_arr[2*j]);
            for(int z=0; z<numlen; ++z){
                putchar(' ');
            }
        }
        putchar('\n');
    }
    for(int j=0; j<num_of_nonzero; ++j){
        printf("%d ", td_len_arr[2*j]);
    }
}

int main(){
    freopen("__input.txt", "r", stdin);
    freopen("__output.txt", "w", stdout);
    
    int len_arr[MAX];
    for(int i=0; i<MAX; ++i){
        len_arr[i] = 0;
    }

    char cr;
    int len = 0;
    int max = 0;
    int last_is_alpha = 0;
    while (1)
    {
        cr=getchar();
        if((cr>='a' && cr<='z')||(cr>='A' && cr<='Z')){//isalpha
            ++len;
            last_is_alpha = 1;
        }else{
            if(last_is_alpha == 1){
                ++len_arr[len];
            }
            if(len_arr[len]>max){
                max = len_arr[len];
            }
            len = 0;
            last_is_alpha = 0;
            if(cr == EOF){
                break;
            }
        }
    }

    for(int i=0; i<MAX; ++i){
        if(len_arr[i] != 0){
            printf("%d: ", i);
            for(int j=0; j<len_arr[i]; ++j){
                putchar('*');
            }
            putchar('\n');//putchar
        }
    }

    printHorizontalHistogram(len_arr,max);
}